(import [flask [render_template send_from_directory]])
(import [koodit [serveri]])

(with-decorator (.route serveri "/css/<path:path>")
  (defn css [path]
    (return (send_from_directory "css" path))))

(with-decorator (.route serveri "/")
  (with-decorator (.route serveri "/indeksi")
    (defn indeksi []
      (render_template "indeksi.html"))))
