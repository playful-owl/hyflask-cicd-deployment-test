{ pkgs ? import <nixpkgs> {} }:
 

pkgs.mkShell {
  buildInputs = [
    pkgs.hy
    pkgs.python37
    pkgs.python37Packages.flask
  ];
}

