(import [flask [Flask]])
(import [config [konffit]])

(setv serveri (Flask __name__))
(serveri.config.from_object konffit)

(import [koodit [reitit]])

